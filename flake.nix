{
  description = "Michaellu's custom dmenu";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";

      overlay = final: prev: {
        dmenu = prev.dmenu.overrideAttrs (old: {
          version = "5.2";
          src = builtins.path { path = ./.; name = "dmenu"; };
        });
      };

      dmenu = (import nixpkgs {
        inherit system;
        overlays = [ overlay ];
      }).dmenu;

    in {
      overlays.default = overlay;

      packages.${system}.default = dmenu;
  };
}
